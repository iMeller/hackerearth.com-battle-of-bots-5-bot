<?php

/**
 * My solution for challenge "Battle of bots #5"
 * @see https://www.hackerearth.com/battle-of-bots-5/multiplayer/reversi/game/575454/
 *
 * @author davidovich.denis@gmail.com
 */
class Board
{
    const GRID_DIMENSION = 10;

    const CELL_STATUS_EMPTY = 0;
    const CELL_STATUS_PLAYER1 = 1;
    const CELL_STATUS_PLAYER2 = 2;
    const CELL_STATUS_ALLOWED = 3;

    public $aEnemy = [
        self::CELL_STATUS_PLAYER1 => self::CELL_STATUS_PLAYER2,
        self::CELL_STATUS_PLAYER2 => self::CELL_STATUS_PLAYER1
    ];

    protected $_board = [];
    protected $_player = 0;
    protected $_enemy = 0;

    /**
     * Stones of players
     *
     * @var array
     */
    protected $_stones = [
        1 => [],
        2 => []
    ];

    /**
     * Step of game [4 - 100]
     * @var int
     */
    protected $_step = 0;

    public function setState($aBoard, $iPlayer) {
        $this->_board = $aBoard;
        $this->_player = $iPlayer;
        $this->_enemy = $this->aEnemy[$iPlayer];

        $this->_stones = [
            $this->_player => $this->collect($this->_player),
            $this->_enemy => $this->collect($this->_enemy),
        ];
        $this->_stones['_'.$this->_player] = count($this->_stones[$this->_player]);
        $this->_stones['_'.$this->_enemy] = count($this->_stones[$this->_enemy]);

        $this->_step = $this->_stones['_'.$this->_player] + $this->_stones['_'.$this->_enemy];

        $this->updateAllowed();
    }

    public function calculate($aBoard, $iPlayer, $aSteps = []) {
        $this->setState($aBoard, $iPlayer);

        //Reader::showBoard($this->_board);

        if(!$aSteps){
            $aSteps = $this->collect(self::CELL_STATUS_ALLOWED);
        }

        $aCalculated = [];
        foreach ($aSteps as $aCells) {
            list($i, $j) = $aCells;
            $iStone = 0;
            $iStone += $this->_calculate($i, $j, 1, 0);
            $iStone += $this->_calculate($i, $j, 1, 1);
            $iStone += $this->_calculate($i, $j, 0, 1);
            $iStone += $this->_calculate($i, $j, -1, 1);
            $iStone += $this->_calculate($i, $j, -1, 0);
            $iStone += $this->_calculate($i, $j, -1, -1);
            $iStone += $this->_calculate($i, $j, 0, -1);
            $iStone += $this->_calculate($i, $j, 1, -1);
            $aCalculated[$iStone][] = $aCells;
        }
        krsort($aCalculated);
        return $aCalculated;
    }

    public function updateAllowed() {
        $aStones = $this->collect($this->_player);
        $this->_resetAllowed();

        //Reader::showBoard($this->_board);

        foreach ($aStones as $aCells) {
            list($i, $j) = $aCells;
            $this->_updateAllowedByDirection($i, $j, 1, 0);  // |
            $this->_updateAllowedByDirection($i, $j, 1, 1);  // \
            $this->_updateAllowedByDirection($i, $j, 0, 1);  // -
            $this->_updateAllowedByDirection($i, $j, -1, 1); // /
            $this->_updateAllowedByDirection($i, $j, -1, 0); // |
            $this->_updateAllowedByDirection($i, $j, -1, -1);// \
            $this->_updateAllowedByDirection($i, $j, 0, -1); // -
            $this->_updateAllowedByDirection($i, $j, 1, -1); // /
        }

        Reader::showBoard($this->_board);
    }

    protected function _resetAllowed() {
        foreach ($this->_board as $i => $aRow) {
            foreach ($aRow as $j => $iCell) {
                if ($iCell === self::CELL_STATUS_ALLOWED) {
                    $this->_board[$i][$j] = self::CELL_STATUS_EMPTY;
                }
            }
        }
    }

    protected function _updateAllowedByDirection($i, $j, $dI, $dJ) {
        $y = $i + $dI;
        $x = $j + $dJ;

        while (1
            && $y > -1
            && $y < self::GRID_DIMENSION
            && $x > -1
            && $x < self::GRID_DIMENSION
            && $this->_board[$y][$x] === $this->_enemy
        ) {
            $y += $dI;
            $x += $dJ;
        }

        if (1
            && $y > -1
            && $y < self::GRID_DIMENSION
            && $x > -1
            && $x < self::GRID_DIMENSION
            && $this->_board[$y - $dI][$x - $dJ] === $this->_enemy
            && ($this->_board[$y][$x] === self::CELL_STATUS_EMPTY
                || $this->_board[$y][$x] === self::CELL_STATUS_ALLOWED
            )
        ) {
            $this->_board[$y][$x] = self::CELL_STATUS_ALLOWED;
        }
    }

    protected function _calculate($i, $j, $dI, $dJ) {
        $y = $i + $dI;
        $x = $j + $dJ;

        while (1
            && $y > -1
            && $y < self::GRID_DIMENSION
            && $x > -1
            && $x < self::GRID_DIMENSION
            && $this->_board[$y][$x] === $this->_enemy
        ) {
            $y += $dI;
            $x += $dJ;
        }

        if (1
            && $y > -1
            && $y < self::GRID_DIMENSION
            && $x > -1
            && $x < self::GRID_DIMENSION
            && $this->_board[$y - $dI][$x - $dJ] === $this->_enemy
            && $this->_board[$y][$x] === $this->_player
        ) {
            return max(abs($y - $i) - 1, abs($x - $j) - 1);
        }

        return 0;
    }

    public function collect($iValue) {
        $aCells = [];
        foreach ($this->_board as $i => $aRow) {
            foreach ($aRow as $j => $iCell) {
                if ($iCell === $iValue) {
                    $aCells[] = [$i, $j];
                }
            }
        }
        return $aCells;
    }

    public function getStones($iPlayer = null){
        return $aStones = $iPlayer
            ? $this->_stones[$iPlayer]
            : $this->_stones
        ;
    }

    public function getStep(){
        return $this->_step;
    }

    public function isPlayerLeads(){
        return $this->_stones['_'.$this->_player] > $this->_stones['_'.$this->_enemy];
    }
}

class Reader
{
    /**
     * Read current step state of game
     */
    public function getState() {
        $board = $this->getBoard();
        $player = $this->getPlayer();

        //Reader::showBoard($board);

        return [
            $board,
            $player,
        ];
    }

    /**
     * Read state of board
     *
     * @return array
     */
    function getBoard() {
        $aBoard = [];
        $n = Board::GRID_DIMENSION;
        $sPattern = str_repeat('%i', Board::GRID_DIMENSION);
        while ($n--) {
            $aBoard[] = fscanf(STDIN, $sPattern);
        }

        return $aBoard;
    }

    /**
     * Get current player Id
     *
     * @return mixed
     */
    function getPlayer() {
        fscanf(STDIN, "%i", $iPlayer);
        return $iPlayer;
    }

    /**
     * Print current state of board
     *
     * @param $aBoard
     * @return bool
     */
    static function showBoard($aBoard) {
        if (!defined('DEBUG_MODE')) {
            return false;
        }

        foreach ($aBoard as $aRow) {
            echo implode(' ', $aRow), "\n";
        }
        echo "\n";

        return true;
    }
}

class Game
{
    static public function run() {
        $reader = new Reader();
        $strategy = new Strategy();

        $aMove = $strategy->move($reader->getState());

        echo implode(' ', $aMove), "\n";
    }
}

class Strategy
{
    /**
     * Current state of board
     *
     * @var array
     */
    public $board = [];

    /**
     * Id Current Player
     *
     * @var int
     */
    public $player = 0;

    /**
     * Make new move.
     *
     * @param $aState [$aBoard, $iPlayer]
     *
     * @return array
     */
    public function move($aState) {

        list($this->board, $this->player) = $aState;

        $oBoard = new Board();

        $oBoard->setState($this->board, $this->player);

        $iStepGame = $oBoard->getStep();

        $aAllowed = $oBoard->collect(Board::CELL_STATUS_ALLOWED);

        #Полный захват
        $iStonesEnemy = count($oBoard->getStones($oBoard->aEnemy[$this->player]));
        $aSteps = $oBoard->calculate($this->board, $this->player, $aAllowed);
        $iStonesCapture = max(array_keys($aSteps));
        if($iStonesCapture === $iStonesEnemy){
            return current($this->getBeaterStep($aSteps));
        }

        #Ходы блокирующие ход пративника (у пративника нет допустимых ходов)
        if ($aStep = $this->findBlockingStep($aSteps)) {
            return $aStep;
        }

        #Основная стратегия
        $aBestStepsByRisk = $this->getBeaterStep($this->estimateRisks($aAllowed));

        if($iStepGame < 10){
            $aMoves = $aAllowed;
        } elseif($iStepGame < 20){
            $aMoves = $aBestStepsByRisk;
        } else {
            $aEstimated = $oBoard->calculate($this->board, $this->player, $aBestStepsByRisk);
            $aDifferences = $this->estimateDifferences($aEstimated);
            $aMoves = $this->getBeaterStep($aDifferences);
        }

        $aMove = $this->_getRandomStep($aMoves);

        return $aMove;
    }

    public function findBlockingStep($aEstimatedSteps){
        $oBoard = new Board();
        foreach ($aEstimatedSteps as $iPlayerPoint => $aSteps) {
            foreach ($aSteps as $aStep) {
                $board = $this->board;
                $board[$aStep[0]][$aStep[1]] = $this->player;

                $oBoard->setState($board, $oBoard->aEnemy[$this->player]);
                $iEnemySteps = count($oBoard->collect(Board::CELL_STATUS_ALLOWED));
                if (!$iEnemySteps) {
                    return $aStep;
                }
            }
        }
        return false;
    }

    public function estimateDifferences($aEstimatedSteps){
        $aDifferences = [];
        $oBoard = new Board();
        foreach ($aEstimatedSteps as $iPlayerPoint => $aSteps) {
            foreach ($aSteps as $aStep) {
                $board = $this->board;
                $board[$aStep[0]][$aStep[1]] = $this->player;
                $aEnemySteps = $oBoard->calculate($board, $oBoard->aEnemy[$this->player]);
                $iEnemyPoint = max(array_keys($aEnemySteps));
                $iDifference = $iPlayerPoint - $iEnemyPoint;
                $aDifferences[$iDifference][] = $aStep;
            }
        }
        return $aDifferences;
    }

    /**
     * Get beater step by key (height value of key beater)
     * 
     * @param $aSteps
     * @return array
     */
    protected function getBeaterStep($aSteps){
        $iStep = max(array_keys($aSteps));
        $aStep = [];
        if (isset($aSteps[$iStep])) {
            $aStep = $aSteps[$iStep];
        }

        return $aStep;
    }

    /**
     * Estimate by risk field
     *
     * @param $aSteps
     * @return array
     */
    public function estimateRisks($aSteps) {
        $oRisks = new Analytics_Risk();
        $aEstimations = [];
        foreach ($aSteps as $aStep) {
            $aEstimations[$oRisks->getValue($aStep)][] = $aStep;
        }
        return $aEstimations;
    }

    /**
     * Get random step
     *
     * @param $aSteps
     * @return array
     */
    protected function _getRandomStep($aSteps) {
        $iStep = rand(0, count($aSteps) - 1);
        return $aSteps[$iStep];
    }
}

class Analytics_Risk
{
    protected $_aRisksMap = [
        [50,  0,  7,  6,  5,  5,  6,  7,  0, 50],
        [ 0,  0,  6,  5,  4,  4,  5,  6,  0,  0],
        [ 7,  6,  5,  4,  3,  3,  4,  5,  6,  7],
        [ 6,  5,  4,  3,  2,  2,  3,  4,  5,  6],
        [ 5,  4,  3,  2,  0,  0,  2,  3,  4,  5],
        [ 5,  4,  3,  2,  0,  0,  2,  3,  4,  5],
        [ 6,  5,  4,  3,  2,  2,  3,  4,  5,  6],
        [ 7,  6,  5,  4,  3,  3,  4,  5,  6,  7],
        [ 0,  0,  6,  5,  4,  4,  5,  6,  0,  0],
        [50,  0,  7,  6,  5,  5,  6,  7,  0, 50],
    ];

    /**
     * Get estimate of step
     *
     * @param array $aStep
     * @return int
     */
    public function getValue($aStep) {
        $iRisk = $this->_aRisksMap[$aStep[0]][$aStep[1]];
        return $iRisk;
    }
}

//define('DEBUG_MODE', 1);
Game::run();